<?php
/* 
Plugin Name: PUT Snippet
Plugin URI: https://catapultthemes.com/
Description: Just a testing plugin for the Plugin Usage Tracker
Version: 0.0.2
Author: Catapult Themes
Author URI: https://catapultthemes.com/
Text Domain: put-snippet
Domain Path: /languages
*/

// Exit if accessed directly

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Stop editing here
if( ! class_exists( 'Plugin_Usage_Tracker') ) {
	require_once dirname( __FILE__ ) . '/tracking/class-plugin-usage-tracker.php';
}


/**
 * Rename the function below so that it is unique and it won't conflict with any other plugins using this tracker
 */
if( ! function_exists( 'put_snippet_start_plugin_tracking' ) ) { 	// Replace function name here
	function put_snippet_start_plugin_tracking() { 					// Replace function name
		$PUTS = new Plugin_Usage_Tracker(
			__FILE__,
			'http://put.catapultthemes.com/',								// Replace with the URL to the site where you will track your plugins
			array(),														// You can specify options here
			true,															// End-user opt-in is required by default
			true															// Include deactivation form
		);
	}
}
add_action( 'init', 'put_snippet_start_plugin_tracking' ); 			// Replace function name


