=== PUT Snippet 2 ===
Contributors: Catapult_Themes
Donate Link: https://catapultthemes.com/
Tags: analytics testing plugin
Requires at least: 4.3
Tested up to: 4.7.1
Stable tag: 0.0.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Just used for testing

== Description ==

N/A

== Installation ==

N/A

== Frequently Asked Questions ==

N/A

== Screenshots ==

N/A

== Changelog ==

= 0.0.1 =

* Initial commit

== Upgrade Notice ==

N/A